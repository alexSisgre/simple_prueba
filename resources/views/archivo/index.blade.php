
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <!-- small box -->
            <div class="tab-content">
                <form class="form-group" method="POST" action="{{ url('/archivo') }}" enctype="multipart/form-data">
                    {{ csrf_field() }} 
                    <div> 
                        <label>nombre</label>
                        <input type="text" name="nombre" class="form-control">
                    </div>
                    <div> 
                        <label>Subir Archivo txt</label>
                        <input type="file" name="archivo_plano" class="form-control">
                    </div><br>
                    <div>
                        <input type="submit" value="Subir archivo" />
                    </div>
                </form>                            
            </div>                    
        </div><!-- ./col -->
    </div>
</section>


