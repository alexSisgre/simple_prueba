<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Archivo;
use App\Encabezado;
use App\Detalle;
class ArchivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('archivo.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('archivo_plano') ){
            $archivo = $request->file('archivo_plano');

            $archivoModel = new Archivo();
            $archivoModel->nombre = $archivo;
            $archivoModel->estado = 1;
            $archivoModel->save();

            $prueba = fopen($archivo,"r") or die("Error al leer archivo");
            //$archivo = Archivo::create($_REQUEST);
            $count = 0;
            while(!feof($prueba)){
                // llenando encabezado
                $linea=fgets($prueba);
                $lineaContent = nl2br($linea);

                $archivo_data = DB::table('archivos')->where('nombre', $archivo)->first();
                $archivo_id = $archivo_data->id;

                // Encabezado del archivo 
                if($count == 0){
                    //$archivo_enb = Archivo::where("nombre","like","%$archivo%")->find(1);
                    
                    $enb_tip_registro = substr($lineaContent,0,2);
                    $enb_modo_plantilla = substr($lineaContent,2,1);
                    $enb_secuencia = substr($lineaContent,3,4);
                    $enb_nom_razonsocial = substr($lineaContent,7,200);
                    $enb_tipo_documento = substr($lineaContent,207,2);
                    $enb_identificacion_aportante = substr($lineaContent,209,16);
                    $enb_digito_verificacion = substr($lineaContent,225,1);

                    $encabezadoModel = new Encabezado();
                    $encabezadoModel->id_archivo = $archivo_id;
                    $encabezadoModel->tipo_registro = $enb_tip_registro;
                    $encabezadoModel->modo_plantilla = $enb_modo_plantilla;
                    $encabezadoModel->secuencia = $enb_secuencia;
                    $encabezadoModel->nombre = $enb_nom_razonsocial;
                    $encabezadoModel->tipo_documento = $enb_tipo_documento; 
                    $encabezadoModel->identificacion_aportante = $enb_identificacion_aportante;
                    $encabezadoModel->digito_verificacion = $enb_digito_verificacion;
                    $encabezadoModel->save();
                    
                }

                // Detalle del archivo 
                if($count > 0){

                    $detalleModel = new Detalle();
                    $det_tip_registro = substr($lineaContent,0,2);
                    $det_modo_plantilla = substr($lineaContent,2,1);
                    $det_secuencia = substr($lineaContent,3,4);
                    $det_tipo_documento = substr($lineaContent,7,2);
                    $det_identificacion_cotizante = substr($lineaContent,9,15);
                    $det_tipo_cotizante = substr($lineaContent,25,2);
                    $det_subtipo_cotizante = substr($lineaContent,27,2);
                    $det_extranjero = substr($lineaContent,29,1); 
                    $det_colombiano_residente = substr($lineaContent,30,1);
                    $det_codigo_departamento = substr($lineaContent,31,2);
                    $det_codigo_municipio = substr($lineaContent,33,3);
                    $det_primer_apellido = substr($lineaContent,36,20);
                    $det_segundo_apellido = substr($lineaContent,56,20);
                    $det_primer_nombre = substr($lineaContent,86,20);
                    $det_segundo_nombre = substr($lineaContent,106,20);
                    
                    $detalleModel->id_archivo = $archivo_id;
                    $detalleModel->tipo_registro = $det_tip_registro;
                    $detalleModel->modo_plantilla = $det_modo_plantilla;
                    $detalleModel->secuencia = $det_secuencia;
                    $detalleModel->tipo_documento = $det_tipo_documento;
                    $detalleModel->identificacion_cotizante = $det_identificacion_cotizante;
                    $detalleModel->tipo_cotizante = $det_tipo_cotizante;
                    if($det_subtipo_cotizante != "" ){$detalleModel->subtipo_cotizante = $det_subtipo_cotizante;}else{$detalleModel->subtipo_cotizante = null; }
                    if($det_extranjero != "" ){$detalleModel->extranjero = ".".$det_extranjero; }else{ $detalleModel->extranjero = null; }
                    if($det_colombiano_residente != "" ){ $detalleModel->residente_exterior =  ".".$det_colombiano_residente; }else{ $detalleModel->residente_exterior = null; }
                    if($det_codigo_departamento != "" ){$detalleModel->codigo_departamento = $det_codigo_departamento;}else{ $detalleModel->codigo_departamento = null; }
                    if($det_codigo_municipio != "" ){ $detalleModel->codigo_municipio = $det_codigo_municipio; }else{ $detalleModel->codigo_municipio = null; }
                    if($det_primer_apellido != "" ){ $detalleModel->primer_apellido = $det_primer_apellido; }else{ $detalleModel->primer_apellido = null; }
                    if($det_segundo_apellido != "" ){$detalleModel->segundo_apellido = $det_segundo_apellido; }else{ $detalleModel->segundo_apellido = null; }
                    if($det_primer_nombre != "" ){ $detalleModel->primer_nombre = $det_primer_nombre; }else{ $detalleModel->primer_nombre = null; }
                    if($det_segundo_nombre != "" ){ $detalleModel->segundo_nombre = ".".$det_segundo_nombre; }else{  $detalleModel->segundo_nombre = null; }
                    $detalleModel->save();
                }
                echo $count." ".$lineaContent;
                $count++;
            } 
        }
        
        // Lectura de arhico plano
        /*  */ 
/*
        $count = 0;
        while(!feof($prueba)){
            // llenando encabezado
            $linea=fgets($prueba);
            $lineaContent = nl2br($linea);
            // llenando encabezado de proyecto
            if($count == 0){
                $archivo_enb = Archivo::find($_REQUEST['id']);
                $enb_tip_registro = substr($lineaContent,0,2);
                $enb_modo_plantilla = substr($lineaContent,2,1);
                $enb_secuencia = "00".substr($lineaContent,3,3);
                $enb_nom_razonsocial = substr($lineaContent,7,199);
                $enb_tipo_documento = substr($lineaContent,207,1);
                $enb_identificacion_aportante = substr($lineaContent,209,15);
                $enb_digito_verificacion = substr($lineaContent,225,1);
                $encabezado = Encabezado::create();
            }else{
                
               
                $detalle = Detalle::create();
            }
            
            echo $saltoLinea;

            $count++;
        }*/

        fclose($prueba);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
