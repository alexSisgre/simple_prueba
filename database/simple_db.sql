-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 29-04-2020 a las 20:39:38
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `simple`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos`
--

CREATE TABLE `archivos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles`
--

CREATE TABLE `detalles` (
  `id` int(11) NOT NULL,
  `id_archivo` int(11) NOT NULL,
  `tipo_registro` varchar(11) NOT NULL,
  `modo_plantilla` varchar(11) NOT NULL,
  `secuencia` varchar(11) NOT NULL,
  `tipo_documento` varchar(11) NOT NULL,
  `identificacion_cotizante` varchar(11) NOT NULL,
  `tipo_cotizante` varchar(255) NOT NULL,
  `subtipo_cotizante` varchar(255) DEFAULT NULL,
  `extranjero` varchar(11) DEFAULT NULL,
  `residente_exterior` varchar(11) DEFAULT NULL,
  `codigo_departamento` varchar(11) DEFAULT NULL,
  `codigo_municipio` varchar(11) DEFAULT NULL,
  `primer_apellido` varchar(255) DEFAULT NULL,
  `segundo_apellido` varchar(255) DEFAULT NULL,
  `primer_nombre` varchar(255) DEFAULT NULL,
  `segundo_nombre` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encabezados`
--

CREATE TABLE `encabezados` (
  `id` int(11) NOT NULL,
  `id_archivo` int(11) NOT NULL,
  `tipo_registro` int(2) NOT NULL,
  `modo_plantilla` int(1) NOT NULL,
  `secuencia` varchar(5) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `tipo_documento` varchar(5) NOT NULL,
  `identificacion_aportante` bigint(18) NOT NULL,
  `digito_verificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `archivos`
--
ALTER TABLE `archivos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalles`
--
ALTER TABLE `detalles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_detalle_archivo` (`id_archivo`);

--
-- Indices de la tabla `encabezados`
--
ALTER TABLE `encabezados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_encabezado_archivo` (`id_archivo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `archivos`
--
ALTER TABLE `archivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `detalles`
--
ALTER TABLE `detalles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `encabezados`
--
ALTER TABLE `encabezados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalles`
--
ALTER TABLE `detalles`
  ADD CONSTRAINT `fk_detalle_archivo` FOREIGN KEY (`id_archivo`) REFERENCES `archivos` (`id`);

--
-- Filtros para la tabla `encabezados`
--
ALTER TABLE `encabezados`
  ADD CONSTRAINT `fk_encabezado_archivo` FOREIGN KEY (`id_archivo`) REFERENCES `archivos` (`id`);
